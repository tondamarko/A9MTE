package com.datadaemon.meteostanice;

import android.app.ActionBar;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.DatePicker;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.datadaemon.meteostanice.components.SwitchingViewPager;
import com.datadaemon.meteostanice.rest.RestClient;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.jjoe64.graphview.series.Series;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

public class TabbedHistoryActivity extends AppCompatActivity implements RestClient.RestHistoryListener, DatePickerDialog.OnDateSetListener {

    private SectionsPagerAdapter mSectionsPagerAdapter;
    private SwitchingViewPager mViewPager;
    private Date date;
    private MenuItem dateMenuItem;
    private SimpleDateFormat sdf;
    private ProgressBar loadingProgressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tabbed_history);

        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        if(actionbar != null)
            actionbar.setDisplayHomeAsUpEnabled(true);

        Calendar cal = Calendar.getInstance();
        date = cal.getTime();

        sdf = new SimpleDateFormat("dd.MM.yyyy");
        loadingProgressBar = findViewById(R.id.loadingProgressBar);
        loadingProgressBar.setVisibility(View.GONE);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        TabLayout tabLayout = findViewById(R.id.tabs);
        mViewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(mViewPager){
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                super.onTabSelected(tab);

                getData();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                super.onTabUnselected(tab);

                int position = tab.getPosition();
                clearFragmentByIndex(position);
            }
        });

        // refresh button floating
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getData();
            }
        });

        RestClient.addHistoryListener(this);

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        boolean scalable = sp.getBoolean(SettingsActivity.SCALABLE_KEY, true);
        // if scalable, then swipe is disabled
        mViewPager.setAllowSwiping(!scalable);

        // load first fragment data
        getData();
    }

    @Override
    protected void onStop() {
        super.onStop();

        // hope it clears loaded data
        clearFragmentByIndex(0);
        clearFragmentByIndex(1);
        clearFragmentByIndex(2);
    }

    /**
     * Calls Rest api for getting data by current tab.
     */
    private void getData(){
        loadingProgressBar.setVisibility(View.VISIBLE);
        int position = mViewPager.getCurrentItem();
        switch(position){
            case 0:
                RestClient.getTemperatureHistory(date, date);
                break;
            case 1:
                RestClient.getHumidityHistory(date, date);
                break;
            case 2:
                RestClient.getPressureHistory(date, date);
                break;
        }
    }

    /**
     * Finds fragment by its index and call clearGraphData.
     * @param index Index of fragment in TabView
     */
    private void clearFragmentByIndex(int index){
        try {
            PlaceholderFragment fragment = (PlaceholderFragment) mSectionsPagerAdapter.getItem(index);
            fragment.clearGraphData();
        }
        catch(Exception ex) {

        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.history, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection

        switch (item.getItemId()) {
            case R.id.menu_item_date:
                setNewDate();
                return true;
            case R.id.menu_item_date_prev:
                addDayToDate(-1);
                return true;
            case R.id.menu_item_date_next:
                addDayToDate(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * Add day count to current history date.
     * @param count Number of days (may be negative)
     */
    private void addDayToDate(int count){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DAY_OF_MONTH, count);
        date = cal.getTime();
        getData();
    }

    /**
     * Creates DatePickerDialog for select date.
     */
    private void setNewDate() {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        DatePickerDialog datePickerDialog = new DatePickerDialog(this, TabbedHistoryActivity.this, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));
        datePickerDialog.show();
    }

    @Override
    public void OnTemperatureHistory(List<RestClient.HistoryItem> history) {
        mSectionsPagerAdapter.tempFragment.setGraphData(history);
        mSectionsPagerAdapter.tempFragment.setGraphTitle(sdf.format(date));
        loadingProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnHumidityHistory(List<RestClient.HistoryItem> history) {
        mSectionsPagerAdapter.humFragment.setGraphData(history);
        mSectionsPagerAdapter.humFragment.setGraphTitle(sdf.format(date));
        loadingProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void OnPressureHistory(List<RestClient.HistoryItem> history) {
        mSectionsPagerAdapter.presFragment.setGraphData(history);
        mSectionsPagerAdapter.presFragment.setGraphTitle(sdf.format(date));
        loadingProgressBar.setVisibility(View.GONE);
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);
        date = cal.getTime();
        getData();
    }

    /**
     * Custom fragment for handling graphs by its type.
     */
    public static class PlaceholderFragment extends Fragment {
        private static final String ARG_TASK_TYPE = "task_type";
        public PlaceholderFragment() {}

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static PlaceholderFragment newInstance(int type) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_TASK_TYPE, type);
            fragment.setArguments(args);
            return fragment;
        }

        GraphView graph;
        int graphColor;
        LineGraphSeries<DataPoint> hist;
        RestClient.RestHistoryAsyncTask.RestTask taskType;
        boolean fixedXAxis;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_tabbed_history, container, false);

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
            boolean scalable = sp.getBoolean(SettingsActivity.SCALABLE_KEY, true);
            fixedXAxis = sp.getBoolean(SettingsActivity.FIXED_X_KEY, false);

            graph = rootView.findViewById(R.id.tab_graph);
            graph.getViewport().setXAxisBoundsManual(true);
            graph.getViewport().setMinX(0);
            graph.getViewport().setMaxX(24);
            graph.getViewport().setScalable(scalable);
            //graph.getViewport().setScalableX(scalable);

            // argument type
            int arg_type =  getArguments().getInt(ARG_TASK_TYPE);
            switch(arg_type){
                case 0:
                    taskType = RestClient.RestHistoryAsyncTask.RestTask.GetTemperatureHistory;
                    graphColor = getResources().getColor(R.color.red);
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinY(0);
                    graph.getViewport().setMaxY(40);
                    break;
                case 1:
                    taskType = RestClient.RestHistoryAsyncTask.RestTask.GetHumidityHistory;
                    graphColor = getResources().getColor(R.color.blue);
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinY(0);
                    graph.getViewport().setMaxY(100);
                    break;
                case 2:
                    taskType = RestClient.RestHistoryAsyncTask.RestTask.GetPressureHistory;
                    graphColor = getResources().getColor(R.color.green);
                    graph.getViewport().setYAxisBoundsManual(true);
                    graph.getViewport().setMinY(700);
                    graph.getViewport().setMaxY(1300);
                    break;
            }
            return rootView;
        }

        /**
         * Converts history item list to LineGraph series for GraphView.
         * @param history Source list from REST.
         * @return Filled LineGraphSeries
         */
        private LineGraphSeries<DataPoint> getLineGraphSeriesFromHistory(List<RestClient.HistoryItem> history){
            DataPoint[] dpArray = new DataPoint[history.size()];
            Calendar calendar = GregorianCalendar.getInstance();
            for(int i = 0; i < history.size(); i++){
                RestClient.HistoryItem hi = history.get(i);

                calendar.setTime(hi.getDate());
                float hour = (float) (calendar.get(Calendar.HOUR_OF_DAY) + (calendar.get(Calendar.MINUTE) / 60f));
                dpArray[i] = new DataPoint(hour, hi.getValue());
            }


            if(!fixedXAxis){
                // get last number from history
                RestClient.HistoryItem last = history.get(history.size()-1);
                calendar.setTime(last.getDate());
                float hour = (float) (calendar.get(Calendar.HOUR_OF_DAY) + (calendar.get(Calendar.MINUTE) / 60f));
                graph.getViewport().setMaxX(hour);
            }
            else{
                graph.getViewport().setMaxX(24);
            }

            return new LineGraphSeries<>(dpArray);
        }

        /**
         * Public method for send history list from REST.
         * @param history Source list from REST
         */
        public void setGraphData(List<RestClient.HistoryItem> history){
            if(history == null || history.size() == 0)
                return;

            if(graph == null)
                return;

            try {
                if (hist != null)
                    graph.removeSeries(hist);
                hist = getLineGraphSeriesFromHistory(history);
                hist.setColor(graphColor);
                graph.addSeries(hist);
            }
            catch (Exception ex){
                ex.printStackTrace();
            }
        }

        /**
         * Clears data and series, for lower memory consumption.
         */
        public void clearGraphData(){
            if(hist != null)
                graph.removeSeries(hist);
            hist = null;
        }

        /**
         * Sets graph title, used for current date.
         * @param title String title
         */
        public void setGraphTitle(String title){
            if(graph != null)
                graph.setTitle(title);
        }

        @Override
        public void onStop() {
            super.onStop();

            // remove all series
            for(Series series : graph.getSeries()){
                graph.removeSeries(series);
            }

            hist = null;
            graph = null;
        }
    }

    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);

            tempFragment = PlaceholderFragment.newInstance(0);
            humFragment = PlaceholderFragment.newInstance(1);
            presFragment = PlaceholderFragment.newInstance(2);
        }

        PlaceholderFragment tempFragment, humFragment, presFragment;

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            //return PlaceholderFragment.newInstance(position + 1);

            switch (position){
                case 0:
                    return tempFragment;
                case 1:
                    return humFragment;
                case 2:
                    return presFragment;
            }

            return null;
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }
    }
}
