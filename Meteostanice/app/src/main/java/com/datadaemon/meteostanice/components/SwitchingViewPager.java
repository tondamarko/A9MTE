package com.datadaemon.meteostanice.components;

import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class SwitchingViewPager extends ViewPager {

    private boolean allowSwiping;

    public SwitchingViewPager(Context context) {
        super(context);

        this.allowSwiping = true;
    }
    public SwitchingViewPager(Context context, AttributeSet attrs){
        super(context, attrs);

        this.allowSwiping = true;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (this.allowSwiping) {
            return super.onTouchEvent(event);
        }

        return false;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.allowSwiping) {
            return super.onInterceptTouchEvent(event);
        }

        return false;
    }

    public void setAllowSwiping(boolean set){
        this.allowSwiping = set;
    }
}
