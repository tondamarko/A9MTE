package com.datadaemon.meteostanice.rest;


import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by Antonín Marko on 10.11.2017.
 */
public class RestClient {

    // default settings
    private static String BASE_URL = "http://meteostanice.jakubsvestka.cz";

    /**
     * Setter for BASE_URL field.
     * @param url New url of REST
     */
    public static void setBaseUrl(String url){
        BASE_URL = url;
    }

    // rest listeners
    private static List<RestListener> listeners;

    /**
     * Register new listener for basic events.
     * @param rl Listener
     */
    public static void addRestListener(RestListener rl){
        if(listeners == null)
            listeners = new ArrayList<RestListener>();

        listeners.add(rl);
    }

    /**
     * Unregister listener from parameter
     * @param rl Listener
     */
    public static void removeListener(RestListener rl){
        if(listeners == null || listeners.isEmpty())
            return;

        int i = listeners.indexOf(rl);
        listeners.remove(i);
    }

    // rest all listeners
    private static List<RestAllListener> allListeners;

    /**
     * Register listener for complex event returning all data.
     * @param rl Listener
     */
    public static void addAllListener(RestAllListener rl){
        if(allListeners == null)
            allListeners = new ArrayList<>();

        allListeners.add(rl);
    }

    /**
     * Unregister listener from parameter.
     * @param rl Listener
     */
    public static void removeListener(RestAllListener rl){
        if(allListeners == null || allListeners.isEmpty())
            return;

        int i = allListeners.indexOf(rl);
        allListeners.remove(i);
    }

    // history listeners
    private static List<RestHistoryListener> historyListeners;

    /**
     * Register listener for history typed events.
     * @param rl Listener
     */
    public static void addHistoryListener(RestHistoryListener rl){
        if(historyListeners == null)
            historyListeners = new ArrayList<RestHistoryListener>();

        historyListeners.add(rl);
    }

    /**
     * Unregister listener from parameter.
     * @param rl Listener
     */
    public static void removeHistoryListener(RestHistoryListener rl){
        if(historyListeners == null || historyListeners.isEmpty())
            return;

        int i = historyListeners.indexOf(rl);
        historyListeners.remove(i);
    }


    private static float _temp, _hum, _pres;
    private static Date _dataValidity;
    /**
     * Checks if current data is up to 3s old, otherwise it is not valid.
     * @return Computed validity.
     */
    private static Boolean checkDataValidity(){
        Date now = new Date();

        // first start
        if(_dataValidity == null)
            return false;

        int ms = (int) (now.getTime() - _dataValidity.getTime());

        // if lower than 3s it is actual
        return (ms < 3000);
    }

    /**
     * Downloads data from REST and fills data to fields.
     */
    private static void loadAllData(){
        JSONObject data = null;
        try {
            data = getJsonObject(BASE_URL + "/?do=getValues");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }


        if(data == null) {
            System.err.println("Nepodarilo se stahnout data");
            return;
        }

        try {
            // {"temperature":24.5,"humidity":34.0,"pressure":983.0,"altitude":252.22}
            _temp = (float) data.getDouble("temperature");
            _hum = (float)data.getDouble("humidity");
            _pres = (float)data.getDouble("pressure");

            // data are valid for 3sec from now
            _dataValidity = new Date();
        }
        catch(Exception ex){
            System.err.printf("LOADALLDATA: %s%n", ex.getMessage());
        }
    }

    // HISTORY FUNCTIONS
    /**
     * Gets Date instance from date string in ISO format.
     * @param date String form of date
     * @return Coverted date.
     */
    private static Date getDateFromString(String date){
        // 2017-11-12T00:00:00+01:00
        // TODO predelat na neco inteligentejsiho

        String part_d, part_t;
        String[] spl = date.split("T");
        part_d = spl[0];
        part_t = spl[1];

        // yyyy-MM-dd
        spl = part_d.split("-");
        int year, month, day, hour, minute, second;

        year = Integer.valueOf(spl[0]);
        month = Integer.valueOf(spl[1]);
        day = Integer.valueOf(spl[2]);

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, month);
        cal.set(Calendar.DAY_OF_MONTH, day);

        // 00:00:00
        part_t = part_t.substring(0, 8);
        spl = part_t.split(":");

        hour = Integer.valueOf(spl[0]);
        minute = Integer.valueOf(spl[1]);
        second = Integer.valueOf(spl[2]);

        cal.set(Calendar.HOUR_OF_DAY, hour);
        cal.set(Calendar.MINUTE, minute);
        cal.set(Calendar.SECOND, second);

        return cal.getTime();
    }

    /**
     * Prepare full link for REST to get history data if specified date interval.
     * @param type Type of history data: temperature, humidity, pressure
     * @param start Date of data start
     * @param end Date of data end
     * @return Link for download from
     */
    private static String prepareLink(String type, Date start, Date end){
        SimpleDateFormat dt = new SimpleDateFormat("dd.MM.yyyy");
        String dateStart = dt.format(start);
        String dateEnd = dt.format(end);
        return String.format("%s/history/%s?do=getValues&from=%s&to=%s", BASE_URL, type, dateStart, dateEnd);
    }

    /**
     * Convert JSONArry to list of HistoryItems. Parse date strings and makes dates.
     * @param array Array from REST.
     * @return Filled list of History items.
     */
    private static List<HistoryItem> convertJsonToHistoryItem(JSONArray array) {
        if(array == null) {
            System.err.println("JSONArray is null");
            return null;
        }
        List<HistoryItem> items = new ArrayList<>();
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
        try {
            for(int i = 0; i < array.length(); i++) {
                JSONObject obj = array.getJSONObject(i);

                Date date = getDateFromString(obj.getString("time"));
                float value = (float) obj.getDouble("value");

                HistoryItem hi = new HistoryItem();
                hi.setDate(date);
                hi.setValue(value);

                items.add(hi);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return items;
    }

    /**
     * Calls REST and download temperature array json string to parse.
     * @param start Start of data
     * @param end End of data
     * @return Filled list of History items.
     */
    private static List<HistoryItem> loadTemperatureHistory(Date start, Date end){
        //http://meteostanice.jakubsvestka.cz/history/temperature?do=getValues&from=3.11.2017&to=10.11.2017
        String fullLink = prepareLink("temperature", start, end);
        List<HistoryItem> items = null;
        try {
            JSONArray array = getJsonArray(fullLink);
            items = convertJsonToHistoryItem(array);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return items;
    }
    /**
     * Calls REST and download humidity array json string to parse.
     * @param start Start of data
     * @param end End of data
     * @return Filled list of History items.
     */
    private static List<HistoryItem> loadHumidityHistory(Date start, Date end){
        String fullLink = prepareLink("humidity", start, end);

        List<HistoryItem> items = null;
        try {
            JSONArray array = getJsonArray(fullLink);
            items = convertJsonToHistoryItem(array);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return items;
    }
    /**
     * Calls REST and download pressure array json string to parse.
     * @param start Start of data
     * @param end End of data
     * @return Filled list of History items.
     */
    private static List<HistoryItem> loadPressureHistory(Date start, Date end){
        String fullLink = prepareLink("pressure", start, end);

        List<HistoryItem> items = null;
        try {
            JSONArray array = getJsonArray(fullLink);
            items = convertJsonToHistoryItem(array);
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return items;
    }
    // HISTORY FUNCTIONS

    /**
     * Opens HTTP connection, asks for data and prepare response string.
     * @param link Prepared link for specific data.
     * @return Response string.
     * @throws IOException I case of some parsing error.
     */
    private static String getResponse(String link) throws IOException {
        String response = "";
        URL url = new URL(link);
        HttpURLConnection connection = (HttpURLConnection)url.openConnection();

        try {
            String line = "";
            InputStream is = connection.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);

            while ((line = br.readLine()) != null) {
                response = "";// String variable declared global
                response += line;
            }
        }
        catch(Exception ex){
            System.err.println(ex.getMessage());
            ex.printStackTrace();
        }
        finally{
            connection.disconnect();
        }

        return response;
    }

    /**
     * Calls getResponse and parse it as JSONObject.
     * @param link Prepared link to REST
     * @return Filled JSONObject
     * @throws JSONException In case of JSON parsing error
     * @throws IOException In case of getResponse error
     */
    private static JSONObject getJsonObject(String link) throws JSONException, IOException {
        String response = getResponse(link);
        if(response.isEmpty())
            return null;
        return new JSONObject(response);
    }
    /**
     * Calls getResponse and parse it as JSONArray.
     * @param link Prepared link to REST
     * @return Filled JSONArray
     * @throws JSONException In case of JSON parsing error
     * @throws IOException In case of getResponse error
     */
    private static JSONArray getJsonArray(String link) throws IOException, JSONException {
        String response = getResponse(link);
        if(response.isEmpty())
            return null;
        return new JSONArray(response);
    }

    /**
     * Extended AsyncTask for getting temperature, humidity, pressure separately. Raises RestListeners.
     */
    public static class RestAsyncTask extends AsyncTask<RestAsyncTask.RestTask, Integer, Float> {

        /**
         * Enum for specific task types called from outside.
         */
        public enum RestTask {
            IsActive,
            GetTemperature,
            GetHumidity,
            GetPressure
        }

        private RestTask currentTask;

        @Override
        protected Float doInBackground(RestTask... restTasks) {
            RestTask task = restTasks[0];
            currentTask = task;
            switch (task) {
                case IsActive:
                    return RestClient.getIsActive();
                case GetTemperature:
                    return RestClient.getCurrentTemperature();
                case GetHumidity:
                    return RestClient.getCurrentHumidity();
                case GetPressure:
                    return RestClient.getCurrentPressure();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Float result){
            super.onPostExecute(result);

            for (RestListener listener : listeners) {
                switch (currentTask) {
                    case IsActive:
                        listener.IsActiveChanged(result > 0f);
                    case GetTemperature:
                        listener.OnTemperatureChanged(result);
                    case GetHumidity:
                        listener.OnHumidityChanged(result);
                    case GetPressure:
                        listener.OnPressureChanged(result);
                }
            }
        }
    }
    /**
     * Extended AsyncTask for getting all data in one call. Raises RestAllListeners.
     */
    public static class RestAllAsyncTask extends AsyncTask<Void, Integer, float[]> {
        @Override
        protected float[] doInBackground(Void... voids) {
            loadAllData();

            return new float[]{_temp, _hum, _pres};
        }

        @Override
        protected void onPostExecute(float[] floats) {
            super.onPostExecute(floats);

            for (RestAllListener ral : allListeners) {
                ral.OnAllReceived(floats[0], floats[1], floats[2]);
            }
        }
    }
    /**
     * Extended AsyncTask for getting history data by specific task. Raises RestHistoryListeners.
     */
    public static class RestHistoryAsyncTask extends AsyncTask<RestHistoryAsyncTask.Task, Integer, List<HistoryItem>> {

        /**
         * Specific task type class, contains task type enum and date interval.
         */
        public static class Task {
            public RestTask Type;
            public Date Start;
            public Date End;
        }

        /**
         * Same as RestAsyncTask.RestTask, but only for variables.
         */
        public enum RestTask {
            GetTemperatureHistory,
            GetHumidityHistory,
            GetPressureHistory
        }

        private Task currentTask;


        @Override
        protected List<HistoryItem> doInBackground(Task... restTasks) {
            Task task = restTasks[0];
            currentTask = task;
            switch (task.Type){
                case GetTemperatureHistory:
                    return loadTemperatureHistory(task.Start, task.End);
                case GetHumidityHistory:
                    return loadHumidityHistory(task.Start, task.End);
                case GetPressureHistory:
                    return loadPressureHistory(task.Start, task.End);
            }
            return null;
        }

        @Override
        protected void onPostExecute(List<HistoryItem> historyItems) {
            super.onPostExecute(historyItems);

            for(RestHistoryListener listener : historyListeners){
                switch(currentTask.Type){
                    case GetTemperatureHistory:
                        listener.OnTemperatureHistory(historyItems);
                        break;
                    case GetHumidityHistory:
                        listener.OnHumidityHistory(historyItems);
                        break;
                    case GetPressureHistory:
                        listener.OnPressureHistory(historyItems);
                        break;
                }
            }
        }
    }


    /**
     * Checks only if getValues returns something, otherwise is REST not active.
     * @return If REST is active.
     */
    private static float getIsActive() {
        try {
            JSONObject obj = getJsonObject(BASE_URL + "/?do=getValues");
            return 1f;
        }
        catch(Exception ex){
            return 0f;
        }
    }

    /**
     * Check data validity and return temperature.
     * @return Current valid temperature.
     */
    private static float getCurrentTemperature(){
        if(!checkDataValidity())
            loadAllData();

        return _temp;
    }
    /**
     * Check data validity and return humidity.
     * @return Current valid humidity.
     */
    private static float getCurrentHumidity(){
        if(!checkDataValidity())
            loadAllData();

        return _hum;
    }
    /**
     * Check data validity and return pressure.
     * @return Current valid pressure.
     */
    private static float getCurrentPressure(){
        if(!checkDataValidity())
            loadAllData();

        return _pres;
    }

    // normal rest func

    /**
     * Creates asynctask for get REST activity.
     * Response in RestListener event.
     */
    public static void isActive(){
        new RestAsyncTask().execute(RestAsyncTask.RestTask.IsActive);
    }

    /**
     * Creates asynctask for get temperature.
     * Response in RestListener event.
     */
    public static void getTemperature(){
        new RestAsyncTask().execute(RestAsyncTask.RestTask.GetTemperature);
    }

    /**
     * Creates asynctask for get humidity.
     * Response in RestListener event.
     */
    public static void getHumidity(){
        new RestAsyncTask().execute(RestAsyncTask.RestTask.GetHumidity);
    }

    /**
     * Creates asynctask for get pressure.
     * Response in RestListener event.
     */
    public static void getPressure(){
        new RestAsyncTask().execute(RestAsyncTask.RestTask.GetPressure);
    }

    /**
     * Creates asynctask for get all data in one event.
     * Response in RestAllListener event.
     */
    public static void getAll(){
        new RestAllAsyncTask().execute();
    }

    // history func

    /**
     * Creates asynctask for get temperature history of specified date interval.
     * Response in RestHistoryListener event.
     * @param start Start of interval
     * @param end End of interval
     */
    public static void getTemperatureHistory(Date start, Date end){
        RestHistoryAsyncTask.Task task = new RestHistoryAsyncTask.Task();
        task.Type = RestHistoryAsyncTask.RestTask.GetTemperatureHistory;
        task.Start = start;
        task.End = end;
        new RestHistoryAsyncTask().execute(task);
    }
    /**
     * Creates asynctask for get humidity history of specified date interval.
     * Response in RestHistoryListener event.
     * @param start Start of interval
     * @param end End of interval
     */
    public static void getHumidityHistory(Date start, Date end){
        RestHistoryAsyncTask.Task task = new RestHistoryAsyncTask.Task();
        task.Type = RestHistoryAsyncTask.RestTask.GetHumidityHistory;
        task.Start = start;
        task.End = end;

        new RestHistoryAsyncTask().execute(task);
    }
    /**
     * Creates asynctask for get pressure history of specified date interval.
     * Response in RestHistoryListener event.
     * @param start Start of interval
     * @param end End of interval
     */
    public static void getPressureHistory(Date start, Date end){
        RestHistoryAsyncTask.Task task = new RestHistoryAsyncTask.Task();
        task.Type = RestHistoryAsyncTask.RestTask.GetPressureHistory;
        task.Start = start;
        task.End = end;

        new RestHistoryAsyncTask().execute(task);
    }

    /**
     * Interface for basic REST events.
     */
    public interface RestListener {
        void IsActiveChanged(Boolean active);
        void OnTemperatureChanged(float temp);
        void OnHumidityChanged(float hum);
        void OnPressureChanged(float pres);
    }

    /**
     * Class of history item for history listener events.
     */
    public static class HistoryItem{
        private Date Date;
        private float Value;

        /**
         * Getter for date.
         * @return Date
         */
        public java.util.Date getDate() {
            return Date;
        }

        /**
         * Setter for date
         * @param date Date to set.
         */
        public void setDate(java.util.Date date) {
            Date = date;
        }

        /**
         * Getter for item value.
         * @return Value of this item
         */
        public float getValue() {
            return Value;
        }

        /**
         * Setter for item value.
         * @param value Value to set.
         */
        public void setValue(float value) {
            Value = value;
        }

        /**
         * Overriden method for getting formated content of item.
         * @return formated string.
         */
        @Override
        public String toString(){
            return String.format("HistoryItem for %s - %f", getDate().toString(), getValue());
        }
    }

    /**
     * Interface for history REST events.
     */
    public interface RestHistoryListener {
        void OnTemperatureHistory(List<HistoryItem> history);
        void OnHumidityHistory(List<HistoryItem> history);
        void OnPressureHistory(List<HistoryItem> history);
    }

    /**
     * Interface for all data REST event.
     */
    public interface RestAllListener {
        void OnAllReceived(float temperature, float humidity, float pressure);
    }
}
