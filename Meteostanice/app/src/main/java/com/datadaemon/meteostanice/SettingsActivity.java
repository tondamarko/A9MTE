package com.datadaemon.meteostanice;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.SwitchPreference;
import android.preference.TwoStatePreference;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Switch;
import android.widget.Toast;

import com.datadaemon.meteostanice.rest.RestClient;

public class SettingsActivity extends AppCompatActivity {

    public static final String BASE_URL_KEY = "url";
    public static final String REFRESH_KEY = "refresh";
    public static final String INTERVAL_KEY = "interval";
    public static final String SCALABLE_KEY = "scalable";
    public static final String FIXED_X_KEY = "fixed_x";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment())
                .commit();

        android.support.v7.app.ActionBar actionBar = getSupportActionBar();
        if(actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
        }
    }

    public static class SettingsFragment extends PreferenceFragment implements RestClient.RestListener, Preference.OnPreferenceClickListener {
        private int SelectedSeconds;
        private Boolean SelectedRefresh;
        private String SelectedUrl;

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);

            // Load the preferences from an XML resource
            addPreferencesFromResource(R.xml.preferences);

            Preference checkAccessibility = findPreference("api_test");
            checkAccessibility.setOnPreferenceClickListener(this);

            EditTextPreference baseUrlPreference = (EditTextPreference) findPreference("base_url");
            baseUrlPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    EditTextPreference etp = (EditTextPreference) preference;
                    String link = etp.getEditText().getText().toString();
                    RestClient.setBaseUrl(link);
                    etp.setSummary(link);
                    SelectedUrl = link;
                    return true;
                }
            });

            SwitchPreference switchPreference = (SwitchPreference) findPreference("autorefresh");
            switchPreference.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    SelectedRefresh = Boolean.valueOf(o.toString());
                    return true;
                }
            });

            ListPreference refreshInterval = (ListPreference) findPreference("refresh_interval");
            refreshInterval.setOnPreferenceChangeListener(new Preference.OnPreferenceChangeListener() {
                @Override
                public boolean onPreferenceChange(Preference preference, Object o) {
                    String val = o.toString();
                    int sec = Integer.valueOf(val) / 1000;
                    SelectedSeconds = sec;
                    preference.setSummary(String.format("%d sec", sec));
                    return true;
                }
            });

            SwitchPreference scalableGraph = (SwitchPreference) findPreference("scalable_graphs");
            SwitchPreference fixedAxis = (SwitchPreference) findPreference("fixed_x_axis");

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
            //SharedPreferences sharedPreferences = getActivity().getSharedPreferences("prefs", 0);
            SelectedUrl = sharedPreferences.getString(BASE_URL_KEY, "");
            SelectedRefresh = sharedPreferences.getBoolean(REFRESH_KEY, false);
            SelectedSeconds = sharedPreferences.getInt(INTERVAL_KEY, 1);

            baseUrlPreference.setText(SelectedUrl);
            baseUrlPreference.setSummary(SelectedUrl);
            switchPreference.setChecked(SelectedRefresh);
            refreshInterval.setValue(String.format("%d", SelectedSeconds * 1000));
            refreshInterval.setSummary(SelectedSeconds + " sec");
            scalableGraph.setChecked(sharedPreferences.getBoolean(SCALABLE_KEY, true));
            fixedAxis.setChecked(sharedPreferences.getBoolean(FIXED_X_KEY, true));
        }

        @Override
        public void onPause() {
            super.onPause();

            SwitchPreference scalableGraph = (SwitchPreference) findPreference("scalable_graphs");
            SwitchPreference fixedAxis = (SwitchPreference) findPreference("fixed_x_axis");

            //SharedPreferences.Editor editor = getActivity().getSharedPreferences("prefs", 0).edit();
            SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(getActivity()).edit();
            editor.putString(BASE_URL_KEY, SelectedUrl);
            editor.putBoolean(REFRESH_KEY, SelectedRefresh);
            editor.putInt(INTERVAL_KEY, SelectedSeconds);
            editor.putBoolean(SCALABLE_KEY, scalableGraph.isChecked());
            editor.putBoolean(FIXED_X_KEY, fixedAxis.isChecked());

            System.out.println(String.format("STORE: %s %d %d", SelectedUrl, SelectedRefresh?1:0, SelectedSeconds));

            if(!editor.commit()){
                Toast.makeText(getActivity(), "Nepodařilo se uložit nastavení", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public boolean onPreferenceClick(Preference preference) {
            preference.setIcon(null);

            RestClient.addRestListener(this);
            RestClient.isActive();
            return false;
        }

        @Override
        public void IsActiveChanged(Boolean active) {
            String text = active
                    ? "Webové API je dostupné"
                    : "Webové API není dostupné";

            Preference pref = findPreference("api_test");
            Resources r = getResources();
            pref.setIcon(r.getDrawable(R.drawable.ic_check_black_24dp));

            Toast.makeText(getActivity().getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            RestClient.removeListener(this);
        }

        @Override
        public void OnTemperatureChanged(float temp) {        }

        @Override
        public void OnHumidityChanged(float hum) {        }

        @Override
        public void OnPressureChanged(float pres) {        }
    }
}
