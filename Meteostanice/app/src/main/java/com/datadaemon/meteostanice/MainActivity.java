package com.datadaemon.meteostanice;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.datadaemon.meteostanice.rest.RestClient;

import java.util.Timer;
import java.util.TimerTask;

import pl.pawelkleczkowski.customgauge.CustomGauge;

public class MainActivity extends AppCompatActivity implements RestClient.RestAllListener {

    TextView tempTextView, humTextView, presTextView;
    CustomGauge tempGauge, humGauge, presGauge;
    Timer refreshTimer;
    TimerTask refreshTimerTask;
    final Handler handler = new Handler();

    private Boolean refreshTimerEnabled = false;
    private int refreshTimerInterval = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tempTextView = findViewById(R.id.temperatureTextView);
        humTextView = findViewById(R.id.humidityTextView);
        presTextView = findViewById(R.id.pressureTextView);

        tempGauge = findViewById(R.id.gauge1);
        humGauge = findViewById(R.id.gauge2);
        presGauge = findViewById(R.id.gauge3);

        //RestClient.addRestListener(this);
        RestClient.addAllListener(this);

        SharedPreferences sp = getSharedPreferences("prefs", 0);
        refreshTimerEnabled = sp.getBoolean(SettingsActivity.REFRESH_KEY, false);
        refreshTimerInterval = sp.getInt(SettingsActivity.INTERVAL_KEY, 1000);

        String url = sp.getString(SettingsActivity.BASE_URL_KEY, "");
        if(!url.isEmpty()){
            RestClient.setBaseUrl(url);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if(!isNetworkAvailable()){
            Snackbar snackbar = Snackbar.make(findViewById(R.id.main_activity_layout), R.string.snack_wifi_unaccessable, Snackbar.LENGTH_INDEFINITE);
            snackbar.setAction(R.string.snack_wifi_turnon, new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(WIFI_SERVICE);
                    if(wifiManager != null)
                        wifiManager.setWifiEnabled(true);
                }
            });
            snackbar.show();
        }

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        refreshTimerEnabled = sp.getBoolean(SettingsActivity.REFRESH_KEY, false);
        refreshTimerInterval = sp.getInt(SettingsActivity.INTERVAL_KEY, 1) * 1000;

        if(refreshTimerEnabled)
            startTimer();
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(refreshTimerEnabled)
            stopTimerTask();
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(refreshTimerEnabled)
            stopTimerTask();
    }

    /**
     * Creates new timer, inits it and starts it.
     */
    private void startTimer(){
        refreshTimer = new Timer();
        initTimerTask();
        refreshTimer.schedule(refreshTimerTask, 0, refreshTimerInterval);
    }

    /**
     * Stops current time if exists.
     */
    private void stopTimerTask(){
        if(refreshTimer != null)
            refreshTimer.cancel();
        refreshTimer = null;
    }

    /**
     * Creates timertask with calling RestClients getAll data, only if network is available.
     */
    private void initTimerTask(){
        refreshTimerTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if(isNetworkAvailable())
                            RestClient.getAll();
                    }
                });
            }
        };
    }

    /**
     * Checks if network is available.
     * @return Availability of network.
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(this.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.menu_item_refresh:
                RestClient.getAll();
                return true;
            case R.id.menu_item_settings:
                Intent settings = new Intent(getApplicationContext(), SettingsActivity.class);
                startActivity(settings);
                return true;
            case R.id.menu_item_graph:
                Intent history = new Intent(getApplicationContext(), TabbedHistoryActivity.class);
                startActivity(history);
                return true;
            case R.id.menu_item_close:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void OnAllReceived(float temperature, float humidity, float pressure) {
        String tempString = getResources().getString(R.string.teplota);
        String humString = getResources().getString(R.string.vlhkost);
        String pressString = getResources().getString(R.string.tlak);

        tempTextView.setText(String.format("%s: %s°C", tempString, temperature));
        tempGauge.setValue((int)temperature);

        humTextView.setText(String.format("%s: %s%%", humString, humidity));
        humGauge.setValue((int)humidity);

        presTextView.setText(String.format("%s: %s hPa", pressString, pressure));
        presGauge.setValue((int)pressure);
    }
}
