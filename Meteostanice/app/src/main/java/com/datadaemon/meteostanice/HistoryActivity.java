package com.datadaemon.meteostanice;

import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.datadaemon.meteostanice.rest.RestClient;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.Date;
import java.util.List;

public class HistoryActivity extends AppCompatActivity implements RestClient.RestHistoryListener {

    LineGraphSeries<DataPoint> tempHist, humHist, presHist;
    GraphView graphView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        ActionBar actionBar = getSupportActionBar();
        if(actionBar != null)
            actionBar.setDisplayHomeAsUpEnabled(true);

        graphView = findViewById(R.id.history_graph);

        Date today = new Date();

        RestClient.addHistoryListener(this);
        RestClient.getTemperatureHistory(today, today);
        RestClient.getHumidityHistory(today, today);
        RestClient.getPressureHistory(today, today);
    }

    @Override
    protected void onStop() {
        super.onStop();

        RestClient.removeHistoryListener(this);
    }

    private LineGraphSeries<DataPoint> getLineGraphSeriesFromHistory(List<RestClient.HistoryItem> history){
        DataPoint[] dpArray = new DataPoint[history.size()];
        for(int i = 0; i < history.size(); i++){
            RestClient.HistoryItem hi = history.get(i);

            dpArray[i] = new DataPoint(hi.getDate().getTime(), hi.getValue());
        }

        return new LineGraphSeries<>(dpArray);
    }

    @Override
    public void OnTemperatureHistory(List<RestClient.HistoryItem> history) {
        graphView.removeSeries(tempHist);
        tempHist = getLineGraphSeriesFromHistory(history);
        graphView.addSeries(tempHist);
    }

    @Override
    public void OnHumidityHistory(List<RestClient.HistoryItem> history) {
        graphView.removeSeries(humHist);
        humHist = getLineGraphSeriesFromHistory(history);
        graphView.addSeries(humHist);
    }

    @Override
    public void OnPressureHistory(List<RestClient.HistoryItem> history) {
        graphView.removeSeries(presHist);
        presHist = getLineGraphSeriesFromHistory(history);
        graphView.addSeries(presHist);
    }
}
